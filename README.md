# tesk-overlay

secretly part of the src_prepare overlay; used as a testing ground for nuclear weapons. sponsored by area 51

## Adding overlay using Layman

    layman -o https://gitlab.com/TheEvilSkeleton/tesk-overlay/raw/master/layman.xml -f -a tesk-overlay
    
## Adding overlay using eselect-repository

    eselect repository add tesk-overlay git https://gitlab.com/TheEvilSkeleton/tesk-overlay.git